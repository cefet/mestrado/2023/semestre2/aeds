

from pathlib import Path
import time
from tsp_heuristic import heuristic
from tsp_loading import load

def show_summary(cities, elapsed, minimal):
    print(f'Total de nós: {cities}\nTempo de execução: {elapsed:e} s')
    print(f'Custo total: {minimal.cost}\nCaminho mínimo: {minimal.path}\n\n')


def heuristic_run(graph):
    begin = time.perf_counter_ns()
    minimal = heuristic(graph)
    end = time.perf_counter_ns()

    elapsed = (end - begin) / 1e9

    show_summary(graph.number_of_nodes(), elapsed, minimal)

    return elapsed


# graph_pa561 = load(filename=Path('data/pa561.tsp'))
# heuristic_run(graph_pa561)

graph_si535 = load(filename=Path('data/si535.tsp'))
# heuristic_run(graph_si535)

graph_si1032 = load(filename=Path('data/si1032.tsp'))
# heuristic_run(graph_si1032)
