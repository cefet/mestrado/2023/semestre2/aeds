"""
Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)
Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Professor: [Thiago de Souza Rodrigues](mailto:thiago.thiago@cefetmg.br)
Estudante: [André Almeida Rocha](mailto:rocha.matcomp@gmail.com)

3ª Avaliação - Trabalho Prático - Problema do caixeiro viajante.

Parte 2 - método heurística
1. Implementar uma heurística para encontrar uma solução para o problema
do caixeiro viajante. A heurística fica a sua escolha.
2. Aplicar o método implementado no item anterior em três instâncias do
problema disponíveis no moodle.
    1. si535.tsp: o problema possui 535 cidades e as distâncias estão
        disponíveis em forma de matriz de adjacência, mas somente a
        diagonal superior desta matriz;
    2. pa561.tsp: o problema possui 561 cidades e as distâncias estão
       disponíveis em forma de matriz de adjacência, mas somente a
       diagonal inferior desta matriz;

    3. si1032.tsp: o problema possui 1032 cidades e as distâncias estão
       disponíveis em forma de matriz de adjacência, mas somente a
       diagonal superior desta matriz;
    4. Verificar a distância calculada pela sua heurística.
"""

from models import Edge, PathCost


def is_visited(graph, node):
    for vertex, visited in graph.nodes.data('visited'):
        if vertex != node:
            continue

        if visited:
            return True

        return False


def set_visited(graph, edge):
    first = graph.nodes.get(edge.first)
    second = graph.nodes.get(edge.second)

    first['visited'] = True
    second['visited'] = True


def show_unvisited(graph):
    for node, visited in graph.nodes.data('visited'):
        if visited:
            continue

        print(node)


def best_edge(graph, node):
    for first, second, weight in graph.edges.data('weight'):
        if is_visited(graph, first) and is_visited(graph, second):
            continue

        if node not in (first, second):
            continue

        yield Edge(first, second, weight)


def greedy(graph, node=0):

    while not all(visited for _, visited in graph.nodes.data('visited')):
        try:
            best = min(best_edge(graph, node), key=lambda edge: edge.weight)
        except ValueError as error:
            print(error)
            print(f'\nNode {node} is problematic!\n')
            breakpoint()
            show_unvisited(graph)

        edge = Edge(
            first=node,
            second=best.first if best.first != node else best.second,
            weight=best.weight
        )

        set_visited(graph, edge)

        node = edge.second

        yield edge


def cost(edges):
    for edge in edges:
        yield edge.weight


def path(edges):
    for edge in edges:
        yield edge.first


def close(graph, edges):
    last = edges[-1].second
    weight = graph.edges.get((0, last))['weight']

    final = Edge(
        last,
        0,
        weight
    )

    edges.append(final)

    total = sum(cost(edges))
    way = list(path(edges))
    way.append(0)

    minimal = PathCost(
        way,
        total
    )

    return minimal


def heuristic(graph):
    edges = list(greedy(graph))
    minimal = close(graph, edges)

    return minimal
