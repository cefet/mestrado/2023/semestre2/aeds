"""
Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)
Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Professor: [Thiago de Souza Rodrigues](mailto:thiago.thiago@cefetmg.br)
Estudante: [André Almeida Rocha](mailto:rocha.matcomp@gmail.com)

3ª Avaliação - Trabalho Prático - Problema do caixeiro viajante.

Parte 1 - método da força bruta:
1. Implementar o método de força bruta para solucionar o problema, ou seja,
um algoritmo que determina todas as possíveis rotas e escolhe a melhor,
ou seja, a menor;
2. Gerar instâncias de tamanho **2** à **n** e aplicar o método implementado
no item 1;
3. Computar o tempo de execução durante a aplicação da força-bruta em cada uma
das instâncias geradas. A aplicação do método deve ser realizada em quantas
instâncias forem possíveis (possivelmente o tamanho máximo vai girar em torno
de 10 a 14 cidades);
"""


from tsp_exhaustive import create_complete_graph, minimal_path
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import time


def show_summary(cities, elapsed, minimal):
    print(f'Total de nós: {cities}\nTempo de execução: {elapsed:e} s')
    print(f'Custo total: {minimal.cost}\nCaminho mínimo: {minimal.path}\n\n')


def exhaustive_run(problem):

    for cities in problem:
        begin = time.perf_counter_ns()
        graph = create_complete_graph(cities)
        minimal = minimal_path(graph)
        end = time.perf_counter_ns()

        elapsed = (end - begin) / 1e9

        show_summary(cities, elapsed, minimal)

        yield elapsed


start = 2
stop = 14

problem = np.arange(start, stop + 1)
times = [elapsed for elapsed in exhaustive_run(problem)]

plt.xlabel("Quantidade de cidades")
plt.ylabel("Tempo (s)")
plt.title("Caixeiro viajante - força bruta")

plt.plot(problem, times)

path = Path(f'caixeiro_viajante_força_bruta_{stop}_cidades.png')
plt.savefig(path)
