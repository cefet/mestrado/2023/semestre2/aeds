"""
Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)
Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Professor: [Thiago de Souza Rodrigues](mailto:thiago.thiago@cefetmg.br)
Estudante: [André Almeida Rocha](mailto:rocha.matcomp@gmail.com)

3ª Avaliação - Trabalho Prático - Problema do caixeiro viajante.

Parte 1 - método da força bruta:
1. Implementar o método de força bruta para solucionar o problema, ou seja,
um algoritmo que determina todas as possíveis rotas e escolhe a melhor,
ou seja, a menor;
2. Gerar instâncias de tamanho **2** à **n** e aplicar o método implementado
no item 1;
3. Computar o tempo de execução durante a aplicação da força-bruta em cada uma
das instâncias geradas. A aplicação do método deve ser realizada em quantas
instâncias forem possíveis (possivelmente o tamanho máximo vai girar em torno
de 10 a 14 cidades);
"""

import itertools
import matplotlib.pyplot as plt
from models import PathCost, HamiltonianCycle
import networkx as nx
import random
from typing import Iterator


def random_weighted_edges(
    total_nodes: int,
    max_weight=100
) -> Iterator[tuple[int, int, int]]:
    """
    Cria arestas com pesos aleatórios.

    Args:
        total_nodes (int): Total de nós do grafo.
        max_weight (int, optional): Peso máximo das arestas. Defaults to 100.

    Yields:
        Iterator[tuple[int, int, int]]:
            Aresta (primeiro nó, segundo nó, peso).
    """
    for first, second in nx.complete_graph(total_nodes).edges():
        yield first, second, random.randrange(1, max_weight)


def create_complete_graph(total_nodes: int, max_weight=100) -> nx.Graph:
    """
    Cria grafo completo ponderado com pesos aleatórios.

    Args:
        total_nodes (int): Total de nós do grafo.
        max_weight (int, optional): Peso máximo das arestas. Defaults to 100.

    Returns:
        nx.Graph: Grafo completo ponderado.
    """
    graph = nx.Graph()
    edges = random_weighted_edges(total_nodes, max_weight)
    graph.add_weighted_edges_from(edges)

    return graph


def show_graph(graph: nx.Graph):
    for fist, second, weight in graph.edges.data('weight'):
        print(f'{fist} -> {second}: {weight}')


def plot_graph(graph: nx.Graph):
    nx.draw_networkx(graph)
    axis = plt.gca()

    plt.axis("off")
    plt.show()


def all_paths(total_nodes: int) -> Iterator[HamiltonianCycle]:
    """
    Define todos os ciclos hamiltonianos a partir do nó 0, ou seja,
    os caminhos passando por todos os outros nós e o primeiro e o
    último nó coincidem.

    Args:
        total_nodes (int): Total de nós do grafo.

    Yields:
        Iterator[HamiltonianCycle]:
            Ciclo hamiltoniano a partir do nó 0.
    """
    # Gera todos os caminhos do meio (sem os nós inicial e final)
    paths = itertools.permutations(
        range(1, total_nodes),
        total_nodes - 1
    )

    for path in paths:
        # Define o caminho como um ciclo hamiltoniano, ou seja,
        # inicializa e finaliza o caminho no mesmo nó 0.
        yield [0, *path, 0]


def paths_weight(
    graph: nx.Graph,
) -> Iterator[PathCost]:
    """
    Calcula

    Args:
        graph (nx.Graph): Grafo completo ponderado.

    Yields:
        Iterator[PathCost]: Caminho com o custo correspondente.
    """
    paths = all_paths(total_nodes=graph.number_of_nodes())

    for path in paths:
        yield PathCost(path, nx.path_weight(graph, path, 'weight'))


def minimal_path(graph: nx.Graph) -> PathCost:
    """
    Busca o caminho mínimo no grafo.

    Args:
        graph (nx.Graph): Grafo completo ponderado.

    Returns:
        PathCost: Caminho mínimo com o custo correspondente.
    """
    return  min(paths_weight(graph))
