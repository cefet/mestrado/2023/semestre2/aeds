

from dataclasses import dataclass
from typing import TypeAlias

# Ciclo hamiltoniano é um caminho no grafo que percorre todos os nós,
# e além disso o primeiro e o último nó coincidem.
HamiltonianCycle: TypeAlias = list[int]


@dataclass(frozen=True)
class Edge:
    first: int
    second: int
    weight: int


@dataclass
class PathCost:
    path: HamiltonianCycle
    cost: int

    def __lt__(self, other):
        return self.cost < other.cost
