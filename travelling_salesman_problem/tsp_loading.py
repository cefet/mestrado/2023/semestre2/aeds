"""
Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)
Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Professor: [Thiago de Souza Rodrigues](mailto:thiago.thiago@cefetmg.br)
Estudante: [André Almeida Rocha](mailto:rocha.matcomp@gmail.com)

3ª Avaliação - Trabalho Prático - Problema do caixeiro viajante.

Parte 2 - método heurística
1. Implementar uma heurística para encontrar uma solução para o problema
do caixeiro viajante. A heurística fica a sua escolha.
2. Aplicar o método implementado no item anterior em três instâncias do
problema disponíveis no moodle.
    1. si535.tsp: o problema possui 535 cidades e as distâncias estão
        disponíveis em forma de matriz de adjacência, mas somente a
        diagonal superior desta matriz;
    2. pa561.tsp: o problema possui 561 cidades e as distâncias estão
       disponíveis em forma de matriz de adjacência, mas somente a
       diagonal inferior desta matriz;

    3. si1032.tsp: o problema possui 1032 cidades e as distâncias estão
       disponíveis em forma de matriz de adjacência, mas somente a
       diagonal superior desta matriz;
    4. Verificar a distância calculada pela sua heurística.
"""

import networkx as nx
from pathlib import Path
import re
from typing import Iterator


def load_metadata(filename: Path) -> dict[str, str]:
    """
    Carrega os metadados a partir do arquivo tsp.

    Args:
        filename (Path): Caminho do arquivo de entrada.

    Returns:
        dict[str, str]: Metadados do arquivo tsp.
    """

    # Metadados do grafo
    metadata = dict()

    with open(filename) as file_input:
        for row_index, row in enumerate(file_input):
            # Procura por uma palavra no início da linha,
            # seguida dois ponto (com um espaços antes ou um depois ou nenhum),
            # e depois por pelo menos um caracter antes do fim da linha.
            result = re.search('^([A-Z_]+)\s?:\s?(.+)\n', row)

            try:
                key = result.group(1).lower()
                value = int(result.group(2))
                metadata[key] = value
            except ValueError:
                # Valor não é um número
                value = result.group(2)
                metadata[key] = value
            except AttributeError:
                # Fim dos metadados: EDGE_WEIGHT_SECTION
                break

    return metadata, row_index


def load_data(
    filename: Path,
    metadata_rows: int
) -> Iterator[tuple[int, int, int]]:
    """


    Args:
        filename (Path): Caminho do arquivo de entrada.
        metadata_rows (int): Total de linhas de metadados.

    Yields:
        Iterator[tuple[int, int, int]]: Arestas ponderadas.
    """
    with open(filename) as file_input:

        for row_index, row in enumerate(file_input):
            for column_index, column in enumerate(row.split()):
                try:
                    weight = int(column)
                except ValueError:
                    # Valor da coluna não é um número
                    # Fim dos dados: DISPLAY_DATA_SECTION ou EOF
                    if column == 'DISPLAY_DATA_SECTION':
                        return

                    break

                if weight == 0:
                    continue

                # Corrige o índice das linhas, saltando a linhas de metadados
                edge = (row_index - metadata_rows - 1, column_index, weight)
                yield edge


def load(filename: Path) -> nx.Graph:
    metadata, metadata_rows = load_metadata(filename)
    graph = nx.Graph(**metadata)

    print(f'Total de nós: {graph.number_of_nodes()}')
    print(f'Total de arestas: {graph.number_of_edges()}')
    print(f'Metadados:\n{graph.graph}\n')

    edges = load_data(filename, metadata_rows)
    graph.add_weighted_edges_from(edges)

    return graph

# graph_pa561 = load(filename='data/pa561.tsp')
# graph_si535 = load(filename='data/si535.tsp')
# graph_si1032 = load(filename='data/si1032.tsp')
