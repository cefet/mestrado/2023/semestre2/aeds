Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)
Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Professor: [Thiago de Souza Rodrigues](mailto:thiago.thiago@cefetmg.br)
Estudante: [André Almeida Rocha](mailto:rocha.matcomp@gmail.com)

3ª Avaliação - Trabalho Prático - Problema do caixeiro viajante.

Parte 1 - método da força bruta:
1. Implementar o método de força bruta para solucionar o problema, ou seja,
um algoritmo que determina todas as possíveis rotas e escolhe a melhor,
ou seja, a menor;
2. Gerar instâncias de tamanho **2** à **n** e aplicar o método implementado
no item 1;
3. Computar o tempo de execução durante a aplicação da força-bruta em cada uma
das instâncias geradas. A aplicação do método deve ser realizada em quantas
instâncias forem possíveis (possivelmente o tamanho máximo vai girar em torno
de 10 a 14 cidades);


> Total de nós: 2
> Tempo de execução: 4.314000e-04 s
> Custo total: 10
> Caminho mínimo: [0, 1, 0]


> Total de nós: 3
> Tempo de execução: 8.750000e-05 s
> Custo total: 200
> Caminho mínimo: [0, 1, 2, 0]


> Total de nós: 4
> Tempo de execução: 2.177000e-04 s
> Custo total: 242
> Caminho mínimo: [0, 1, 2, 3, 0]


> Total de nós: 5
> Tempo de execução: 1.950000e-04 s
> Custo total: 199
> Caminho mínimo: [0, 1, 2, 4, 3, 0]


> Total de nós: 6
> Tempo de execução: 1.069600e-03 s
> Custo total: 153
> Caminho mínimo: [0, 1, 3, 2, 5, 4, 0]


> Total de nós: 7
> Tempo de execução: 4.654800e-03 s
> Custo total: 225
> Caminho mínimo: [0, 1, 2, 6, 3, 5, 4, 0]


> Total de nós: 8
> Tempo de execução: 3.779100e-02 s
> Custo total: 270
> Caminho mínimo: [0, 4, 6, 2, 3, 5, 1, 7, 0]


> Total de nós: 9
> Tempo de execução: 3.371992e-01 s
> Custo total: 113
> Caminho mínimo: [0, 1, 8, 7, 6, 2, 3, 5, 4, 0]


> Total de nós: 10
> Tempo de execução: 3.207565e+00 s
> Custo total: 250
> Caminho mínimo: [0, 1, 5, 8, 2, 4, 3, 9, 6, 7, 0]


> Total de nós: 11
> Tempo de execução: 3.597903e+01 s
> Custo total: 213
> Caminho mínimo: [0, 4, 3, 9, 8, 10, 2, 7, 1, 5, 6, 0]


> Total de nós: 12
> Tempo de execução: 4.228077e+02 s
> Custo total: 145
> Caminho mínimo: [0, 4, 10, 11, 2, 7, 8, 3, 1, 6, 9, 5, 0]


> Total de nós: 13
> Tempo de execução: 5.400793e+03 s
> Custo total: 199
> Caminho mínimo: [0, 3, 9, 12, 8, 7, 10, 5, 6, 11, 1, 2, 4, 0]


> Total de nós: 14
> Tempo de execução: 7.968981e+04 s
> Custo total: 171
> Caminho mínimo: [0, 10, 8, 1, 7, 3, 13, 4, 11, 5, 6, 2, 9, 12, 0]
