# AEDS - Algoritmos e Estrutura de Dados [MMC.43]

Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)

Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Professor: [Thiago de Souza Rodrigues](mailto:thiago.thiago@cefetmg.br)

Estudante: [André Almeida Rocha](mailto:rocha.matcomp@gmail.com)


## 3ª Avaliação - Trabalho Prático

**Objetivo:** Implementar algoritmo de força bruta e uma heurística para o problema do Caixeiro Viajante.

O trabalho é composto de duas partes, aplicação do algoritmo de **força-bruta** em instâncias do problema do caixeiro viajante e aplicação de uma **heurística** em três instâncias do mesmo problema.

O problema do Caixeiro Viajante consiste em, dado um conjunto de cidades onde existe um
caminho entre cada par de cidade com uma distância positiva, encontrar um caminho que, a partir de uma cidade, visita-se todas as cidades e retorna à cidade inicial percorrendo a menor distância possível.

**Parte 1:**
1. Implementar o método de força bruta para solucionar o problema, ou seja, um algoritmo que determina todas as possíveis rotas e escolhe a melhor, ou seja, a menor;
2. Gerar instâncias de tamanho **2** à **n** e aplicar o método implementado no item 1;
3. Computar o tempo de execução durante a aplicação da força-bruta em cada uma das instâncias geradas. A aplicação do método deve ser realizada em quantas instâncias forem
possíveis (possivelmente o tamanho máximo vai girar em torno de 10 a 14 cidades);

**Observação:** as instâncias devem ser geradas de forma automática onde os pesos possuem valores aleatórios. Pode-se utilizar qualquer tipo de representação de grafos que se desejar.

**Parte 2:**
1. Implementar uma **heurística** para encontrar uma solução para o problema do caixeiro viajante. A heurística fica a sua escolha.
2. Aplicar o método implementado no item anterior em três instâncias do problema disponíveis no moodle (aqui disponível em [data](./data)):
    1. **si535.tsp:** o problema possui **535** cidades e as distâncias estão disponíveis em forma de matriz de adjacência, mas somente a **diagonal superior** desta matriz;
    2. **pa561.tsp**: o problema possui **561** cidades e as distâncias estão disponíveis em forma de matriz de adjacência, mas somente a **diagonal inferior** desta matriz;
    3. **si1032.tsp:** o problema possui **1032** cidades e as distâncias estão disponíveis em forma de matriz de adjacência, mas somente a **diagonal superior** desta matriz;
    4. Verificar a distância calculada pela sua heurística.

**O que deve ser entregue:**
- Postar no moodle o código fonte das implementações, **comentados**;
- Relatório **via moodle** contendo:
    - Gráfico mostrando o crescimento exponencial do tempo necessário para resolver o caixeiro viajante pelo crescimento do tamanho do problema – utilizando o **força-bruta**;
    - Mostrar a distância encontrada pela heurística para cada uma das instâncias disponíveis no moodle;
